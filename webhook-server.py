#!/usr/bin/env python2

"""
A lightweight HTTPS server for use with GitLab webhooks

source: https://gitlab.uvm.edu/saa/gitlab-webhooks
author: Brian O'Donnell <brian.odonnell@uvm.edu>
"""

from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
from SocketServer import ForkingMixIn
import json
import re
import ssl
import subprocess
import sys
import fabric.api
import logging
import smtplib # For sending deploy emails
import time # For getting the current time when sending deploy emails

# Customize this string before use!!!
SECRET_KEY_FILE = '/users/w/e/webmster/gitlab-webhooks/secret-key'

# Port to bind the http server to
LISTEN_PORT = 8000

# Path to SSL certificate
CERT_FILE = '/users/w/e/webmster/gitlab-webhooks/webhook-server.pem'

def loggingConfig():
    """ Static loggingConfig. """
    logging.basicConfig(level=logging.INFO,
                            filename="drupal-deploy.log", filemode="a+",
                            format="%(asctime)-15s %(levelname)-8s %(message)s")


class WebhookHandler(BaseHTTPRequestHandler):
    """
    An inheritable class which implements a basic GitLab webhooks API
    """
    def exec_push(self, data):
        raise NotImplementedError('exec_push')

    def exec_tag_push(self, data):
        raise NotImplementedError('exec_tag_push')

    def exec_note(self, data):
        raise NotImplementedError('exec_note')

    def exec_issue(self, data):
        raise NotImplementedError('exec_issue')

    def exec_merge_request(self, data):
        raise NotImplementedError('exec_merge_request')

    def exec_build(self, data):
        raise NotImplementedError('exec_build')

    def respond(self, code, message):
        """
        Sends a customizable HTTP response
        """
        self.send_response(code)
        self.send_header("Content-type", "text")
        self.send_header("Content-length", str(len(message)))
        self.end_headers()
        self.wfile.write(message)

    def do_POST(self):
        """
        Handles POST requests
        """
        try:
            # read in the secret key
            scf = open(SECRET_KEY_FILE, "r")
            secret_key = scf.readline()
            if secret_key.endswith("\n"):
                secret_key = secret_key[:-1]
            # slurp in the request body
            self.rfile._sock.settimeout(5)
            content = self.rfile.read(int(self.headers['Content-Length']))
            postdata = json.loads(content)

            # parse out the 'secret key' from the url path
            m = re.match('\/([A-Za-z0-9]+)\/?$', self.path)

            if m:
                # Force the user to set a custom secret key
                if secret_key == 'SUPERSECRETALPHANUMERICSTRING' or secret_key == '' or secret_key is None:
                    raise ValueError('Please set a secret key!')

                if m.group(1) != secret_key:
                    logging.info("secret key [%s] did not match [%s]" % (m.group(1), secret_key))
                    self.respond(400, 'Invalid secret key')
                elif 'object_kind' in postdata:
                    action = postdata['object_kind']

                    if action == 'push':
                        self.respond(200, 'OK')
                        self.exec_push(postdata)
                    elif action == 'tag_push':
                        self.respond(200, 'OK')
                        self.exec_tag_push(postdata)
                    elif action == 'issue':
                        self.respond(200, 'OK')
                        self.exec_issue(postdata)
                    elif action == 'note':
                        self.respond(200, 'OK')
                        self.exec_note(postdata)
                    elif action == 'merge_request':
                        self.respond(200, 'OK')
                        self.exec_merge_request(postdata)
                    elif action == 'build':
                        self.respond(200, 'OK')
                        self.exec_build(postdata)
                    else:
                        self.respond(400, 'Invalid object_kind attribute')
                        return

                else:
                    self.respond(400, 'Missing object_kind attribute')
            else:
                self.respond(400, 'Missing or invalid secret key in url')
        except NotImplementedError as e:
            self.respond(501, 'Method %s not implemented' % e)
        except (RuntimeError, ValueError) as e:
            self.respond(500, '%s' % e)




class CustomHookHandler(WebhookHandler):
    """
    Override parent class' exec_ methods here
    """

    def run(self, *args, **kwargs):
        loggingConfig()
        cmd = args[0]
        logging.info("About to run command: %s" % cmd)
        with fabric.api.settings(warn_only=True):
            rv = fabric.api.run(*args, **kwargs)
        logging.info("local command returned %s" % rv)
        logging.info("rv return_code: %s" % rv.return_code)
        if not rv.succeeded:
            logging.info(rv.stdout)
            logging.info(rv.stderr)
            logging.error("Run command did not succeed, exiting request handler.")
            raise WHFabricException(rv.stderr)

    def local(self, *args, **kwargs):
        loggingConfig()
        cmd = args[0]
        logging.info("About to run local() command: %s" % cmd)
        kwargs['capture'] = True
        with fabric.api.settings(warn_only=True):
            rv = fabric.api.local(*args, **kwargs)
        logging.info("local command returned %s" % rv)
        #logging.info("rv dir: %s" % dir(rv))
        logging.info("rv return_code: %s" % rv.return_code)
        if not rv.succeeded:
            logging.info(rv.stdout)
            logging.info(rv.stderr)
            logging.error("Local command did not succeed, exiting request handler.")
            raise WHFabricException(rv.stderr)

    def execute(self, *args, **kwargs):
        loggingConfig()
        cmd = args[0]
        logging.info("About to run execute() command: %s" % cmd)
        with fabric.api.settings(warn_only=True):
            rd = fabric.api.execute(*args, **kwargs)
        logging.info("execute rd is %s, dir is %s" % (rd, dir(rd)))

    def update_git_files(self, branch, after, alias):
        logging.info("I want to autodeploy a change to newrev: %s in environment: %s" % (after, branch))
        self.run('git --git-dir=/var/www/%s.drup-lb.uvm.edu/.git fetch neworigin' % alias)
        self.run('git --work-tree=/var/www/{alias}.drup-lb.uvm.edu/ --git-dir=/var/www/{alias}.drup-lb.uvm.edu/.git checkout neworigin/{branch}'.format(alias=alias, branch=branch))
        self.run('git --work-tree=/var/www/%s.drup-lb.uvm.edu/ --git-dir=/var/www/%s.drup-lb.uvm.edu/.git reset --hard' % (alias, alias))

    def update_db(self, env):
        subprocess.call('drush @%s sql-drop --yes' % env, shell=True)
        test = subprocess.call('drush sql-sync @prod @%s --yes' % env, shell=True)
        logging.info(test)

    def exec_tag_push(self, data):
        logging.info("Saw a tag push for tag %s" % data["ref"])

        # If the tag is not a "uvm-###" tag, do no deploy it
        deploytag = data["ref"].find("refs/tags/uvm-") == 0
        nomaintain = data["commits"][-1]["message"].find("#nomaint") == -1

        if not deploytag:
                logging.info("Tag name %s does not start with 'uvm-', not deploying it" % data["ref"])
        else:
                alias = "prod"
                branch = "production"
                after = data['checkout_sha']

                logging.info("Sending deploy email")
                self.__send_deploy_email(data)

                logging.info("Clearing cache")
                self.local('drush @%s cc all' % alias)

                if nomaintain:
                    logging.info("Entering maintenance mode")
                    self.local('drush @%s vset maintenance_mode 1' % alias)
                else:
                    logging.info("#nomaint in top commit, not entering maintenance mode")

                logging.info("Performing git checkout to commit %s" % after)
                results = self.execute(self.update_git_files, "production", after, alias, hosts=['drup-ws1', 'drup-ws2'])

                logging.info("Performing database updates")
                self.local('drush @%s updatedb --yes' % alias)

                logging.info("Exiting maintenance mode")
                self.local('drush @%s vset maintenance_mode 0' % alias)

                logging.info("Clearing cache")
                self.local('drush @%s cc all' % alias)

                logging.info("Sucessfully finished deployment to Production")

    def __update_test_instance(self, instance, commit, syncdb, nomaint):
        """
        Updates an instance's database and files to live, and code to
        the given commit.
        """
        branch = instance

        loggingConfig()
        logging.info("--------")
        logging.info("Going to deploy to %s" % instance)

        if syncdb:
            logging.info("Syncing database from production to %s" % instance)
            self.update_db(instance)
        else:
            logging.info("#nosyncdb in top commit, not syncing database")

        if nomaint:
            logging.info("Entering maintenance mode on %s" % instance)
            self.local('drush @%s vset maintenance_mode 1' % instance)
        else:
            logging.info("#nomaint in top commit, not entering maintenance mode")

        logging.info("Updating git repository on %s" % instance)
        results = self.execute(self.update_git_files, branch, commit, instance, hosts=['localhost'])

        logging.info("Running database updates on %s" % instance)
        self.local('drush @%s updatedb --yes' % instance)

        if nomaint:
            logging.info("Exiting maintenance mode on %s" % instance)
            self.local('drush @%s vset maintenance_mode 0' % instance)

        logging.info("Clearing cache on %s" % instance)
        self.local('drush @%s cc all' % instance)

        logging.info("Syncing files on %s" % instance)
#        subprocess.call('rsync -r --progress --delete webmster@drup-ws1.uvm.edu:/var/www/prod.drup-lb.uvm.edu/sites/default/files /var/www/%s.drup-lb.uvm.edu/sites/default/' % instance, shell=True)
	subprocess.call('rsync -av --no-owner --itemize-changes --delete webmster@drup-ws1.uvm.edu:/var/www/prod.drup-lb.uvm.edu/sites/default/files /var/www/%s.drup-lb.uvm.edu/sites/default/' % instance, shell=True)

        logging.info("Sucessfully Finished deployment to %s" % instance)

    def exec_push(self, data):
        branch = data["ref"].replace("refs/heads/", "")
        after = data["after"]
        # If the "#syncdb" string is in the commit message, tell the deploy
        # function to sync the database. (by default, we don't.)
        syncdb = not data["commits"][-1]["message"].find("#syncdb") == -1
        
        # If the "#nomaint" string is in the commit message, tell the deploy
        # function not to enter maintenance mode.
        nomaint = data["commits"][-1]["message"].find("#nomaint") == -1

        if branch in ["staging", "qa"]:
            self.__update_test_instance(branch, after, syncdb, nomaint)
        else:
            logging.info("Saw a push to branch %s, ignoring it" % branch)

    def __send_deploy_email(self, data):
        # Get the new tag, knowing that we can't get here unless it starts with "uvm-"
        ref = data["ref"]
        tag = ref[ref.find("uvm-"):]
        # Get the old tag
        oldtag = "uvm-" + str(int(tag[4:]) - 1)
        # gitlab does not tell us the time of the push(?), so use the current time
        currtime = time.ctime()

        link = "https://gitlab.uvm.edu/webteam/uvm-drupal/compare/" + oldtag + "..." + tag
        user = data["user_name"]

        sender = "Drupal Deploy Script <webmster@uvm.edu>"
        to = ["UVM Web Team <webteam@uvm.edu>"]
        message = """\
From: %s
To: %s
Subject: Drupal Deploy %s

At %s, %s was deployed by %s. View the list of changes:
%s
""" % (sender, ", ".join(to), tag, currtime, tag, user, link)

        server = smtplib.SMTP("localhost")
        server.sendmail(sender, ", ".join(to), message)
        server.quit()


class WHFabricException(Exception):
	def __init__(self, msg):
		self.msg = msg


class ForkingHTTPServer(ForkingMixIn, HTTPServer):
    pass

class LoggingHTTPServer(ForkingHTTPServer):

    def handle_error(self, request, client_address):
        loggingConfig()
        logging.info("Error in handling request from client %s: %s" % (request, client_address))
        #logging.info("directory of request: %s" % dir(request))
        exceptArray = sys.exc_info()
        logging.info("Exception %s, Value %s" % (exceptArray[0], exceptArray[1]))
        import traceback
        logging.info(traceback.format_exc())
        sys.exc_clear()

    def log_message(self, fmt, *args):
        loggingConfig()
        logging.error(fmt % args)


def main():
    """
    Starts the webserver
    """
    try:
        loggingConfig()
        logging.info("Starting webhook server")
        server = LoggingHTTPServer(('', LISTEN_PORT), CustomHookHandler)
        server.socket = ssl.wrap_socket(server.socket, certfile=CERT_FILE, server_side=True)
        logging.info("Server started, %s" % server)
        fabric.api.env.abort_exception = WHFabricException
        server.serve_forever()
    except KeyboardInterrupt:
        logging.info('Ctrl-C pressed, shutting down.')
    except:
        e = sys.exc_info()[0]
        v = sys.exc_info()[1]
        logging.info("Error: %s" % e )
        logging.info("Value: %s" % v )
	
    finally:
        sys.exit(1)

if __name__ == '__main__':
    main()

# vim: set et sw=4 ts=4:
