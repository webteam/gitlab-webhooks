# Description
A lightweight HTTPS server for use with GitLab Web hooks

This script is intended to run as a standalone web-server to handle requests
from [GitLab](https://gitlab.com)'s webhooks framework. The implementation
details of the hooks themselves are left to the user to provide.

The `WebhookHandler` class provides stubs for the six types of webhook triggers:
* exec_push
* exec_tag_push
* exec_note
* exec_issue
* exec_merge_request
* exec_build

These methods may be overridden in the `CustomHookHandler` class. Each is
expected to accept as an argument a dictionary containing the deserialized
webhook request content, and raise a `RuntimeError` on failure.

Example:

```python
class CustomHookHandler(WebhookHandler):
    def exec_push(self, content):
        try:
            send_email_to(content['user_email'])
        except:
            raise RuntimeError('Something went wrong!')
```

Please see GitLab's [webhook documentation](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/web_hooks/web_hooks.md) for more information.

# Instructions for Use
Generate an SSL certificate. The provided `generate_certificate.sh` has been
included to facilitate creating a self-signed certificate for testing purposes,
but a valid cert from a trusted CA is preferred for production use.

Edit the script to include customized web hook logic, set the `SECRET_KEY`
variable to an alphanumeric string, and point `CERTFILE` at your SSL
certificate, then run `python webhook-server.py`.

In the GitLab web interface, navigate to 'Web Hooks' in your projects Settings
menu. Set the URL to *https://somehost:8000/SECRET_KEY* and enable the
triggers you would like to use. If you are using a self-signed SSL cert, be
sure that 'Enable SSL verification' is not checked.
